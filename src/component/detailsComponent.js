import React, {Component} from 'react';

// import {NavigationContainer, StackActions} from '@react-navigation/native';

// import {createStackNavigator} from '@react-navigation/stack';

// import {IconButton, Colors} from 'react-native-paper';

import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  StatusBar,
} from 'react-native';

// import {Searchbar} from 'react-native-paper';

import Header from 'react-native-elements';

// const Stack = createStackNavigator();

export default class DetailsComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
    };
  }
  render() {
    return (
      <View>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#8bc34a"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        {/* <Header
          leftComponent={{icon: 'menu', color: '#fff'}}
          centerComponent={{text: 'MY TITLE', style: {color: '#fff'}}}
          rightComponent={{icon: 'home', color: '#fff'}}
        /> */}

        <Image
          source={require('../image/fashio1.jpg')}
          style={{height: 200, width: 300, marginLeft: 30, marginTop: 10}}
        />
        <Text style={{marginTop: 20}}>This is Category page</Text>
      </View>
    );
  }
}
