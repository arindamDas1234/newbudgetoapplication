import React, {Component} from 'react';

import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Profile from '../screens/Profile';
import Logout from '../screens/Logout';
import searchLocation from '../screens/SearchLocation';
import {AsyncStorage} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Products from '../screens/products';
import Cart from '../screens/cart';
import categoryProducts from '../screens/productWithCategory';
// const {navigation} = this.props;

const Drawer = createDrawerNavigator();

// function CustomerDrawerContente(props) {
//   return (
//     <DrawerContentScrollView {...props}>

//     </DrawerContentScrollView>
//   )
// }

export default class DrawerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLocation: '',
    };
  }

  // componentWillMount() {}

  componentWillMount() {}

  render() {
    return (
      <Drawer.Navigator>
        <Drawer.Screen
          name="Home"
          component={Profile}
          options={{
            drawerIcon: () => <Icon name="home" size={20} color="#9e9e9e" />,
          }}
        />
        <Drawer.Screen
          name="Logout"
          component={Logout}
          options={{
            drawerIcon: () => (
              <Icon name="sign-out" size={20} color="#9e9e9e" />
            ),
          }}
        />
        <Drawer.Screen
          name="Select Location"
          component={searchLocation}
          options={{
            drawerIcon: () => (
              <Icon name="location-arrow" size={20} color="#9e9e9e" />
            ),
          }}
        />
        <Drawer.Screen
          name="Products"
          component={Products}
          options={{
            drawerIcon: () => (
              <Icon name="shopping-basket" size={20} color="#9e9e9e" />
            ),
          }}
        />
        <Drawer.Screen
          name="cart"
          component={Cart}
          options={{
            drawerIcon: () => (
              <Icon name="shopping-cart" size={20} color="#9e9e9e" />
            ),
          }}
        />
      </Drawer.Navigator>
    );
  }
}
