import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
} from 'react-native';

import {IconButton, Colors} from 'react-native-paper';

import {Header} from 'react-native-elements';

import {Searchbar} from 'react-native-paper';

import SearchableDropdown from 'react-native-searchable-dropdown';

import NetInfo from '@react-native-community/netinfo';

var locations = [];

export default class SearchLocation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locationName: '',
      shopDetails: [],
    };
  }

  componentWillMount() {
    this.delete();
    this.getlocation();
  }

  delete = async () => {
    await AsyncStorage.getItem('location_id').then(result => {
      if (result) {
        AsyncStorage.removeItem('location_id');
      }
    });
  };

  // showList = () => {
  //   locationName.forEach(result => {
  //     shopLocation.push(result);
  //   });
  // };

  // data = () => {
  //   console.log(shopLocation);
  // };

  sendLocation = () => {
    // NetInfo.fetch().then(state => {
    //   if (state.isConnected) {
    //     fetch('http://vmi317167.contaboserver.net/ecommerce/api/stores_list', {
    //       method: 'POST',
    //       headers: {
    //         'Content-Type': 'application/x-www-form-urlencoded',
    //         Accept: 'application/json',
    //       },
    //       body: 'location_id=' + this.state.locationName,
    //     })
    //       .then(response => response.json())
    //       .then(result => {
    //         console.log(result);
    //         return;
    //         // console.log(this.state.locationName);
    //         // return;
    //         this.setState({shopDetails: result});
    //         try {
    //           AsyncStorage.getItem('location_id').then(value => {
    //             AsyncStorage.removeItem('location_id');
    //             AsyncStorage.setItem('location_id', this.state.locationName);
    //             this.props.navigation.navigate('Home');
    //           });
    //         } catch (error) {}
    //       })
    //       .catch(error => {
    //         console.log(error);
    //       });
    //   } else {
    //     alert('Please Check Your Internet connection');
    //   }
    // });
    var shop_token = this.state.locationName;
    this.props.navigation.navigate('Home', {
      value: shop_token,
    });
  };

  getlocation = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/shop/locations',
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            // console.log(result.data.area_name);
            // this.setState({
            //   selectLocation: [
            //     this.state.selectLocation,
            //     result.data.area_name,
            //   ],
            // });
            // console.log(result);
            for (let i = 0; i < result.data.length; i++) {
              const element = result.data[i];
              // console.log(element);
              const data = {
                id: element.id,
                name: element.area_name,
              };
              console.log(data);
              locations.push(data);
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        alert('Please Check You Internet Connection');
      }
    });
  };
  render() {
    return (
      <View style={styles.container}>
        {/* {console.log(locations)} */}
        <Header
          //   placement="left"
          leftComponent={
            <IconButton
              icon="menu"
              color={Colors.white}
              size={20}
              onPress={() => this.props.navigation.openDrawer()}
            />
          }
          centerComponent={{
            text: 'Select Location',
            style: {color: '#fff', fontSize: 18, fontFamily: 'lucida grande'},
          }}
          containerStyle={{
            backgroundColor: '#689f38',
            justifyContent: 'space-around',
          }}
        />

        {/* <TouchableOpacity onPress={() => this.data()}>
          <Text>Touch</Text>
        </TouchableOpacity> */}

        <Image
          source={require('../image/modelIcon.png')}
          style={{height: 200, width: 200, marginTop: 20}}
        />
        <Text
          style={{fontSize: 20, color: '#9e9e9e', fontFamily: 'lucida grande'}}>
          Select your existing Location
        </Text>

        <SearchableDropdown
          onTextChange={text => console.log(text)}
          //On text change listner on the searchable input
          onItemSelect={items => {
            this.setState({locationName: items.id});
          }}
          //onItemSelect called after the selection from the dropdown
          containerStyle={{padding: 5}}
          //suggestion container style
          textInputStyle={{
            //inserted text style
            padding: 12,
            borderWidth: 1,
            borderColor: '#ccc',
            backgroundColor: '#FAF7F6',
            marginTop: 30,
            width: 300,
          }}
          itemStyle={{
            //single dropdown item style
            padding: 10,
            marginTop: 10,
            backgroundColor: '#FAF9F8',
            borderColor: '#bbb',
            borderWidth: 1,
          }}
          itemTextStyle={{
            //text style of a single dropdown item
            color: '#222',
          }}
          itemsContainerStyle={{
            //items container style you can pass maxHeight
            //to restrict the items dropdown hieght
            maxHeight: '60%',
          }}
          items={locations}
          //mapping of item array
          defaultIndex={0}
          //default selected item index
          placeholder="Select your Location"
          //place holder for the search input
          resetValue={false}
          //reset textInput Value with true and false state
          underlineColorAndroid="transparent"
          //To remove the underline from the android input
        />

        <TouchableOpacity
          onPress={() => this.sendLocation()}
          style={{
            backgroundColor: '#689f38',
            marginTop: 20,
            width: 150,
            height: 35,
          }}>
          <Text
            style={{
              marginTop: 6,
              textAlign: 'center',
              color: '#fff',
              fontSize: 16,
            }}>
            Continue
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
