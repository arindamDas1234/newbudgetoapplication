import React, {Component} from 'react';
import {ActivityIndicator, View, StyleSheet, AsyncStorage} from 'react-native';

export default class Loading extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLocation: '',
    };
  }
  componentWillMount() {
    this.checkLocation();
  }

  checkLocation = async () => {
    let location = await AsyncStorage.getItem('user_location');
    this.state.isLocation = location;

    if (this.state.isLocation == null) {
      setTimeout(() => {
        this.props.navigation.replace('LocationSelection');
      }, 1000);
    }
  };

  check;
  render() {
    return (
      <View style={styles.load}>
        <ActivityIndicator size="large" color="#8bc34a" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  load: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
