import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Picker,
} from 'react-native';

import {TextInput} from 'react-native-paper';

import {Button} from 'react-native-paper';

import {RadioButton} from 'react-native-paper';

import DatePicker from 'react-native-datepicker';

import NetInfo from '@react-native-community/netinfo';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: '',
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      confirmPassword: '',
      date: '',
      number: '',
    };
  }

  //  email validation

  validationEmail = email => {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  show = value => {
    // alert(value);
    this.setState({gender: value});
  };

  //  fetch api for Register

  Register = () => {
    // // console.log('firstname=' + this.state.firstname);
    // return;
    // console.log(this.state.password);
    // return;
    if (
      this.state.firstname == '' ||
      this.state.lastname == '' ||
      this.state.email == '' ||
      this.state.password == '' ||
      this.state.date == '' ||
      this.state.confirmPassword == '' ||
      this.state.number == ''
    ) {
      alert('Pleace Fill The form Field');
    } else {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          if (this.state.password != this.state.confirmPassword) {
            alert('Password did not matched Currectly');
          } else if (!this.validationEmail(this.state.email)) {
            alert('Email Formate is Invalid Pleace Check Your email');
          } else {
            try {
              fetch(
                'http://vmi317167.contaboserver.net/ecommerce/api/customer_register',
                {
                  method: 'POST',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                  },
                  body:
                    'first_name=' +
                    this.state.firstname +
                    '&last_name=' +
                    this.state.lastname +
                    '&mobile=' +
                    this.state.number +
                    '&email=' +
                    this.state.email +
                    '&password=' +
                    this.state.password +
                    '&password_confirmation=' +
                    this.state.confirmPassword +
                    '&gender=' +
                    this.state.gender +
                    '&dob=' +
                    this.state.date +
                    '&receive_offer=' +
                    0 +
                    '&news_letter_subscribe=' +
                    1,
                },
              )
                .then(response => response.json())
                .then(res => {
                  // console.log(res.msg);
                  // return;
                  if (res.data) {
                    alert(res.data);
                    this.props.navigation.replace('Login');
                  }
                })
                .catch(error => {
                  console.log(error);
                });
            } catch (error) {
              console.log(error);
            }
          }
        } else {
          alert('Pleace Check Your Internet connection ');
        }
      });
    }
  };

  //   updateGender = value => {
  //     this.state.gender = value;
  //   };
  render() {
    return (
      <View style={styles.ViewContainer}>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#8bc34a"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        <ScrollView>
          <KeyboardAvoidingView>
            <Text style={styles.HeaderText}>Register New Account</Text>

            <TextInput
              label="First name"
              onChangeText={firstname => this.setState({firstname})}
              style={styles.InputField}
            />
            <TextInput
              label="Last name"
              onChangeText={lastname => this.setState({lastname})}
              style={styles.InputField}
            />
            <TextInput
              label="Mobile Number"
              onChangeText={number => this.setState({number})}
              style={styles.InputField}
            />
            <TextInput
              label="Email"
              onChangeText={email => this.setState({email})}
              style={styles.InputField}
            />
            <TextInput
              label="Password"
              secureTextEntry={true}
              onChangeText={password => this.setState({password})}
              style={styles.InputField}
            />
            <TextInput
              label="Confirm password"
              secureTextEntry={true}
              onChangeText={confirmPassword => this.setState({confirmPassword})}
              style={styles.InputField}
            />
            <Picker
              style={{height: 20, width: 300, marginBottom: 20, elevation: 5}}
              selectedValue={this.state.gender}
              onValueChange={this.show.bind()}>
              <Picker.Item label="Male" value="Male" />
              <Picker.Item
                label="Female"
                value="Female"
                // onChangeText={female => female}
              />
            </Picker>
            <DatePicker
              style={{width: 300, marginBottom: 30}}
              date={this.state.date}
              mode="date"
              placeholder="select date"
              format="MM/DD/YYYY"
              minDate="12/1/1990"
              maxDate="12/1/2000"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 30,
                  top: 4,
                  marginLeft: 0,
                },
                dateInput: {
                  marginRight: 4,
                },
                // ... You can check the source to find the other keys.
              }}
              onDateChange={date => {
                this.setState({date: date});
              }}
            />

            {/* <TextInput label="Password" style={styles.InputField} /> */}

            <Button
              mode="contained"
              style={styles.ButtonField}
              onPress={this.Register}>
              Signin
            </Button>

            <TouchableOpacity
              onPress={() => this.props.navigation.replace('Login')}>
              <Text style={styles.OpacityText}>
                Allready have Any Account ? Signin
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  HeaderText: {
    paddingTop: 40,
    fontSize: 30,
    fontWeight: 'normal',
    marginBottom: 15,
    paddingLeft: 20,
  },
  InputField: {
    width: 300,
    marginBottom: 25,
    height: 45,
  },
  ButtonField: {
    width: 300,
    borderRadius: 3,
    elevation: 5,
    backgroundColor: '#8bc34a',
  },
  OpacityText: {
    paddingTop: 15,
    color: '#4a148c',
    paddingLeft: 30,
  },
});
