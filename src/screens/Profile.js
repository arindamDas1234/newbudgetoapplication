import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Image,
  Picker,
  AsyncStorage,
  ScrollView,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import Modal from 'react-native-modal';

import {Button} from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

// import Carousel from 'react-native-carousel-view';

import {SliderBox} from 'react-native-image-slider-box';

import {Card} from 'react-native-elements';

import {Header} from 'react-native-elements';

import {IconButton, Colors, Badge} from 'react-native-paper';

import {Searchbar} from 'react-native-paper';

// const {navigation} = this.props;

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      search: '',
      shops: [],
      cartCount: [],
      sliderImage: [
        require('../image/fashio1.jpg'),
        require('../image/newFshion5.png'),
        require('../image/newFshion6.png'),
      ],
      featureSlider: [
        require('../image/fImage1.jpg'),
        require('../image/fimage2.jpg'),
        require('../image/fimage4.jpg'),
        require('../image/fimge2.jpg'),
      ],
    };
  }

  updateSearch = value => {
    this.setState({search: value});
  };

  showLocation = value => {
    this.setState({location: value});
  };

  // componentWillMount() {
  //   // console.log(this.state.search);
  //   this.getShops();
  //   // this.setState({shops: []});
  // }

  componentWillMount() {
    this.getShops();
  }

  // componentWillUpdate() {
  //   AsyncStorage.getItem('location_id').then(result => {
  //     if (result == this.props.route.params.value) {
  //       this.updateShops();
  //     }
  //   });
  // }

  getShops = async () => {
    let token_shop = await AsyncStorage.getItem('location_id');
    console.log(token_shop);

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        fetch('http://vmi317167.contaboserver.net/ecommerce/api/stores_list', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: 'location_id=' + token_shop,
        })
          .then(response => response.json())
          .then(res => {
            console.log(res);
            this.setState({shops: res});
          });
      } else {
        alert('Please check Your Internet Connection');
      }
    });
  };

  updateShops = async () => {
    let token_shop = await AsyncStorage.getItem('location_id');
    console.log(token_shop);

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        fetch('http://vmi317167.contaboserver.net/ecommerce/api/stores_list', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: 'location_id=' + token_shop,
        })
          .then(response => response.json())
          .then(res => {
            // console.log(res);
            this.setState({shops: res});
          });
      } else {
        alert('Please check Your Internet Connection');
      }
    });
  };

  shopDetails = () => {};
  render() {
    const {search} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#8bc34a"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        <ScrollView>
          <Header
            placement="left"
            leftComponent={
              <IconButton
                icon="menu"
                color={Colors.white}
                size={20}
                onPress={() => this.props.navigation.openDrawer()}
              />
            }
            centerComponent={
              <Searchbar
                placeholder="Search"
                style={{
                  width: 200,
                  // height: 40,
                  // borderRadius: 5,
                  marginTop: 5,
                  marginBottom: 2,
                }}
                value={this.state.search}
                onChangeText={value => this.setState({search: value})}
              />
            }
            rightComponent={
              <TouchableOpacity
                onPress={() => {
                  console.log('success');
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="shopping-cart" size={20} color="#fff" />
                  <View>
                    <Badge>1</Badge>
                  </View>
                </View>
              </TouchableOpacity>
            }
            containerStyle={{
              backgroundColor: '#689f38',
              justifyContent: 'space-around',
            }}
          />

          {/* <TouchableOpacity onPress={() => this.idStore()}>
          <Text>Hi</Text>
        </TouchableOpacity> */}

          <SliderBox
            images={this.state.sliderImage}
            style={{height: 200, width: 360, marginTop: 0, marginLeft: 4}}
            autoplay
            paginationBoxVerticalPadding={20}
            sliderBoxHeight={200}
          />
          <Text
            style={{
              fontSize: 20,
              marginTop: 2,
              marginBottom: 10,
              paddingHorizontal: 20,
            }}>
            Featured Products
          </Text>
          <SliderBox
            images={this.state.featureSlider}
            style={{height: 100, width: 360, marginTop: 1, marginLeft: 2}}
            autoplay
            circleLoop
            paginationBoxVerticalPadding={20}
            sliderBoxHeight={200}
          />

          {/* <Image
          source={require('../image/newFshion1.png')}
          style={{ height: 130, width: 160, marginTop: 50 }}
        />
        <Text style={{marginTop:20}}>This is Product Title</Text> */}
          <View style={styles.childContainer}>
            {this.state.shops ? (
              Object.assign(this.state.shops).map(result => {
                console.log(result.vendorInfo.shop_name);
                return (
                  <View>
                    <View>
                      <Image
                        source={require('../image/newFshion5.png')}
                        style={{
                          height: 130,
                          width: 160,
                          marginTop: 50,
                          marginLeft: 10,
                          borderRadius: 5,
                          elevation: 10,
                        }}
                      />
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 20,
                          paddingBottom: 10,
                          marginTop: 10,
                        }}>
                        {result.vendorInfo.shop_name}
                      </Text>

                      <Button
                        style={{
                          backgroundColor: '#689f38',
                          width: 150,
                          marginLeft: 18,
                          borderRadius: 10,
                        }}
                        mode="contained"
                        onPress={() => {
                          result.vendorInfo.id
                            ? this.props.navigation.navigate('Products', {
                                value: result.vendorInfo.id,
                              })
                            : alert('alert');
                        }}>
                        select
                      </Button>
                    </View>
                  </View>
                );
              })
            ) : (
              <Text>No Shops Ware Found</Text>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection:'row'
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  iconImage: {
    width: 2,
    height: 20,
    marginTop: 10,
    paddingLeft: 4,
    marginRight: 4,
    paddingRight: 20,
    marginLeft: 6,
  },
  slideContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slide1: {
    backgroundColor: 'rgba(20,20,200,0.3)',
  },
  slide2: {
    backgroundColor: 'rgba(20,200,20,0.3)',
  },
  slide3: {
    backgroundColor: 'rgba(200,20,20,0.3)',
  },
  childContainer: {
    flexDirection: 'row',
  },
});
