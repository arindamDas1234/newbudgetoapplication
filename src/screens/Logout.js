import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  StatusBar,
} from 'react-native';

export default class Logout extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.logout();
  }

  logout = async () => {
    setTimeout(async () => {
      try {
        await AsyncStorage.getItem('user_token').then(value => {
          AsyncStorage.removeItem('user_token');
          this.props.navigation.replace('Login');
        });
      } catch (error) {}
    }, 2000);
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#4a148c"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />
        <ActivityIndicator size="large" color="#4a148c" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
