import React, {Component, Fragment} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  Picker,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';

import NetInfo from '@react-native-community/netinfo';

import {TextInput} from 'react-native-paper';

import {Button} from 'react-native-paper';

import SearchableDropdown from 'react-native-searchable-dropdown';

// import { NavigationActions } from '@react-navigation';

// import {useNavigation} from '@react-navigation/native';

// const navigation = useNavigation();

var Location = [];
var namelocation = [];

export default class LocationSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectLocation: '',
      locationName: '',
      isLoading: false,
      shops: [],
    };
  }

  componentWillMount() {
    this.getLocation();
    // this.LocationName.bind();
  }

  // componentDidUpdate() {
  //   this.LocationName();
  // }

  // LocationName = () => {
  //   Location.map(result => {
  //     for (let i = 0; i < result.length; i++) {
  //       const element = result[i];
  //       namelocation.push(element.area_name);
  //     }
  //   });
  // };

  getLocation = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/shop/locations',
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            for (let i = 0; i < result.data.length; i++) {
              const element = result.data[i];
              const shopName = {
                id: element.id,
                name: element.area_name,
              };
              Location.push(shopName);
            }
          });
      } else {
        alert('No Internet connection');
      }
    });
  };
  showLocation = value => {
    this.setState({selectLocation: value});
  };

  saveLocation = () => {
    var location_Value = this.state.locationName;
    console.log(location_Value);
    try {
      AsyncStorage.setItem('location_id', location_Value.toString());

      setTimeout(() => {
        this.state.isLoading == true;
        this.props.navigation.replace('DrawerContainer');
      }, 1000);
    } catch (error) {}
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#8bc34a"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />

        <Image
          source={require('../image/modelIcon.png')}
          style={{height: 150, width: 300, marginTop: 100}}
        />
        <Text
          style={{
            textAlign: 'center',
            fontSize: 15,
            paddingTop: 10,
            color: '#9e9e9e',
          }}>
          Select your Existing Location
        </Text>

        <SearchableDropdown
          onTextChange={text => console.log(text)}
          //On text change listner on the searchable input
          onItemSelect={items => {
            this.setState({locationName: items.id});
          }}
          //onItemSelect called after the selection from the dropdown
          containerStyle={{padding: 5}}
          //suggestion container style
          textInputStyle={{
            //inserted text style
            padding: 12,
            borderWidth: 1,
            borderColor: '#ccc',
            backgroundColor: '#FAF7F6',
            marginTop: 30,
            width: 300,
          }}
          itemStyle={{
            //single dropdown item style
            padding: 10,
            marginTop: 10,
            backgroundColor: '#FAF9F8',
            borderColor: '#bbb',
            borderWidth: 1,
          }}
          itemTextStyle={{
            //text style of a single dropdown item
            color: '#222',
          }}
          itemsContainerStyle={{
            //items container style you can pass maxHeight
            //to restrict the items dropdown hieght
            maxHeight: '60%',
          }}
          items={Location}
          //mapping of item array
          defaultIndex={0}
          //default selected item index
          placeholder="Select your Location"
          //place holder for the search input
          resetValue={false}
          //reset textInput Value with true and false state
          underlineColorAndroid="transparent"
          //To remove the underline from the android input
        />

        <TouchableOpacity onPress={() => this.saveLocation()}>
          {this.state.isLoading == true ? (
            <ActivityIndicator color="#8bc34a" size="large" />
          ) : (
            <Text
              style={{
                backgroundColor: '#8bc34a',
                width: 150,
                marginTop: 20,
                fontSize: 20,
                color: '#fff',
                height: 40,
                textAlign: 'center',
                paddingTop: 3,
                borderWidth: 2,
                borderColor: '#8bc34a',
                borderRadius: 3,
                elevation: 5,
              }}>
              Continue
            </Text>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
  },
});
