import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button,
  StatusBar,
  AsyncStorage,
} from 'react-native';

import {Header, ListItem, List, Icon, Avatar} from 'react-native-elements';
// import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'

import {Searchbar} from 'react-native-paper';

import {IconButton, Colors, Badge} from 'react-native-paper';

import IconBadge from 'react-native-icon-badge';

import NetInfo from '@react-native-community/netinfo';

export default class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      Details: [],
      image: '',
      totalPrices: '',
      product_id: '',
      featureProducts: [
        {
          productName: 'Potao',
          image:
            'https://5.imimg.com/data5/QU/HD/MY-46065548/red-capcicum-500x500.jpg',
          price: '20',
        },
        {
          productName: 'Potao',
          image:
            'https://cdnp.iconscout.com/photo/free/thumb/bringle-isolated-on-a-white-background-1852096-1569449.jpg',
          price: '20',
        },
        {
          productName: 'Potao',
          image:
            'https://image.shutterstock.com/image-photo/carrot-isolated-on-white-background-260nw-475001332.jpg',
          price: '20',
        },
        {
          productName: 'Potao',
          image:
            'https://image.shutterstock.com/image-photo/cabbage-isolated-on-white-background-260nw-571780078.jpg',
          price: '20',
        },
      ],
    };
  }
  ProductDetail = [];

  componentWillMount() {
    this.getProduct();
  }

  // shouldComponentUpdate() {
  //   this.newProduct();
  // }

  getProduct = async () => {
    console.log(this.state.product_id);
    await AsyncStorage.getItem('product_slug').then(result => {
      if (result) {
        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            fetch(
              'http://vmi317167.contaboserver.net/ecommerce/api/vendor/product/slug/' +
                result,
              {
                method: 'GET',
                headers: {
                  'Content-Type': 'application/json',
                },
              },
            )
              .then(response => response.json())
              .then(result => {
                console.log(result.prices[0].id);
                // console.log(result.image.large);
                // this.setState({Details: result});
                this.setState({Details: result});
                this.setState({image: result.image.large});
                this.setState({totalPrices: result.prices[0].total});
                this.setState({product_id: result.prices[0].product_id});
                // await AsyncStorage.removeItem('product_slug');
              })
              .catch(error => {
                console.log(error);
              });
          } else {
            alert('Please Check Your Internet Connection');
          }
        });
      }
    });
  };

  // newProduct = async () => {
  //   await AsyncStorage.getItem('product_slug').then(result => {
  //     NetInfo.fetch().then(state => {
  //       if (state.isConnected) {
  //         fetch(
  //           'http://vmi317167.contaboserver.net/ecommerce/api/vendor/product/slug/' +
  //             result,
  //           {
  //             headers: {
  //               'Content-Type': 'application/text',
  //             },
  //             method: 'GET',
  //           },
  //         )
  //           .then(response => response.json())
  //           .then(result => {
  //             for (let i = 0; i < result.length; i++) {
  //               const element = result[i];
  //               console.log(element);
  //             }
  //             // console.log(result);
  //             this.setState({Details: result});
  //             // this.setState({Details: result});
  //           })
  //           .catch(error => {
  //             console.log(error);
  //           });
  //       } else {
  //         alert('Please Check Your Internet Connection');
  //       }
  //     });
  //   });
  // };

  clickEventListener() {
    AsyncStorage.setItem({product_id: this.state.product_id});
    Alert.alert('Success', 'Please Go to Your Cart Page');
    this.props.navigation.navigate('DrawerContainer');
  }

  // viewProduct = () => {
  //   console.log(this.state.Details.image.small);
  // };

  render() {
    return (
      <View>
        {/* {console.log(this.state.image)} */}
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#8bc34a"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />

        <Header
          placement="right"
          leftComponent={
            <IconButton
              icon="home"
              color={Colors.white}
              size={20}
              onPress={() => this.props.navigation.replace('DrawerContainer')}
            />
          }
          centerComponent={
            <IconButton
              icon="cart"
              color={Colors.white}
              size={20}
              onPress={() => this.props.navigation.replace('DrawerContainer')}
            />
          }
          // rightComponent={<Badge>3</Badge>}
          containerStyle={{
            backgroundColor: '#689f38',
            justifyContent: 'space-around',
          }}
        />

        <ScrollView>
          <View style={{alignItems: 'center', marginHorizontal: 30}}>
            <Image style={styles.productImg} source={{uri: this.state.image}} />
            <Text style={styles.name}>{this.state.Details.title_en}</Text>
            <Text style={styles.price}> Price ${this.state.totalPrices}</Text>
            <Text style={styles.description}>
              {this.state.Details.description_en}
            </Text>
          </View>
          <Text
            style={{
              fontSize: 20,
              marginLeft: 20,
              paddingTop: 10,
              fontFamily: 'lucida grande',
            }}>
            Feature Products
          </Text>

          <FlatList
            showsHorizontalScrollIndicator={false}
            style={{width: '100%', marginTop: 10}}
            horizontal
            data={this.state.featureProducts}
            renderItem={(item, index) => {
              // console.log(this.state.product_id);
              return (
                <View>
                  <View>
                    <Image
                      source={{uri: item.item.image}}
                      style={{
                        height: 200,
                        width: 200,
                        justifyContent: 'space-around',
                        marginLeft: 15,
                      }}
                    />
                    <View>
                      <Text
                        style={{
                          textAlign: 'center',
                          paddingTop: 4,
                          fontSize: 20,
                        }}>
                        {item.item.productName}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
          />

          <View style={styles.separator} />
          <View style={styles.addToCarContainer}>
            <TouchableOpacity
              style={styles.shareButton}
              onPress={() => this.clickEventListener()}>
              <Text style={styles.shareButtonText}>Add To Cart</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  productImg: {
    width: 200,
    height: 200,
  },
  name: {
    fontSize: 20,
    color: '#696969',
    fontWeight: 'bold',
  },
  price: {
    marginTop: 4,
    fontSize: 18,
    color: 'green',
    fontWeight: 'bold',
  },
  description: {
    textAlign: 'center',
    marginTop: 4,
    color: '#696969',
  },
  star: {
    width: 40,
    height: 40,
  },
  btnColor: {
    height: 30,
    width: 30,
    borderRadius: 30,
    marginHorizontal: 3,
  },
  btnSize: {
    height: 40,
    width: 40,
    borderRadius: 40,
    borderColor: '#778899',
    borderWidth: 1,
    marginHorizontal: 3,
    backgroundColor: 'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  contentColors: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  contentSize: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  separator: {
    height: 2,
    backgroundColor: '#eeeeee',
    marginTop: 20,
    marginHorizontal: 30,
  },
  shareButton: {
    marginTop: 5,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#689f38',
  },
  shareButtonText: {
    color: '#FFFFFF',
    fontSize: 20,
  },
  addToCarContainer: {
    marginHorizontal: 30,
  },
});
