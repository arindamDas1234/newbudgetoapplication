import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  AsyncStorage,
} from 'react-native';

export default class Splash extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLogin: '',
      isNotified: '',
      isLocation: '',
    };
  }
  componentWillMount() {
    this.getData();
  }

  getData = async () => {
    let id = await AsyncStorage.getItem('user_token');

    this.state.isLogin = id;

    setTimeout(() => {
      if (this.state.isLogin == null) {
        this.props.navigation.replace('Login');
      } else {
        this.props.navigation.replace('LoadingScreen');
      }
    }, 2000);
  };

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../image/budgeto.png')}
          style={{width: '100%', height: '100%', resizeMode: 'contain'}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
  },
});
