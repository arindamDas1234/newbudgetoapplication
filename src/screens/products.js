import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  Image,
  FlatList,
  Dimensions,
  SafeAreaView,
  AsyncStorage,
  Icon,
} from 'react-native';

import {Header, ListItem, List} from 'react-native-elements';

// import {} from  'react-native-elements'

import {IconButton, Colors} from 'react-native-paper';

import {Searchbar} from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

export default class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
      shop_id: '',
      products: [],
      productsCategory: [],
    };
  }
  componentWillMount() {
    this.vendorInfo();
    if (this.props.route.params.value != '') {
      this.setState({shop_id: this.props.route.params.value});

      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          fetch(
            'http://vmi317167.contaboserver.net/ecommerce/api/vendor/products?vendor_id=' +
              this.state.shop_id,
            {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
              },
            },
          )
            .then(response => response.json())
            .then(result => {
              this.setState({products: result.data.data});
              Object.assign(result.data).map(value => {});
            });
        } else {
          alert('Please Check Your Internet Connection');
        }
      });
    } else {
      this.props.navigation.nanvigate('Profile');
    }
  }

  vendorInfo = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/vendor/categories?vendor_id=3',
          {
            method: 'GET',
            headers: {
              'Contente-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            this.setState({productsCategory: result.data});
          })
          .then(error => {
            console.log(error);
          });
      } else {
        alert('Please Check your Internet Connection');
      }
    });
  };

  componentWillReceiveProps() {
    this.vendorInfo();
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/vendor/products?vendor_id=' +
            this.props.route.params.value,
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            this.setState({products: result.data.data});
            // console.log(result.data);
            // Object.assign(result.data.prices).forEach(value => {
            //   console.log(value);
            // });
            // Object.assign(result.data).map(result => {});
          });
      } else {
        alert('Please Connect Your Internet Connection');
      }
    });
  }

  handleOnPressProductList = async data => {
    await AsyncStorage.setItem('product_slug', data);
    this.props.navigation.replace('DetailsPage');
  };

  productsWithCategory = data => {
    console.log(data);
  };
  render() {
    return (
      <View style={style.container}>
        {/* {console.log(this.state.products)} */}
        <StatusBar
          barStyle="light-content"
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          backgroundColor="#8bc34a"
          //Background color of statusBar
          translucent={false}
          //allowing light, but not detailed shapes
          networkActivityIndicatorVisible={true}
        />

        <FlatList
          horizontal
          // horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{marginBottom: 20}}
          data={this.state.productsCategory}
          renderItem={(items, index) => {
            return (
              <View>
                <TouchableOpacity
                  onPress={() =>
                    this.productsWithCategory(items.item.category.id)
                  }>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#757575',
                      marginLeft: 20,
                      paddingTop: 10,
                    }}>
                    {items.item.category.name_en}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          }}
        />
        {/* <Flatlist
          horizontal={true}
          style={{flex:1}}
        /> */}

        {/* <FlatList
          data={this.state.products}
          renderItem={items => {
            // console.log(items.item.quantity);
            return (
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={{uri: items.item.image.small}}
                  style={{height: 120, width: 140, marginTop: 10}}
                />
                <View>
                  <Text style={{marginLeft: 8, marginTop: 20}}>
                    {items.item.title_en}
                  </Text>
                  <Text style={{marginLeft: 8, marginTop: 20}}>
                    {items.item.prices[0].size}
                  </Text>
                </View>
              </View>
            );
          }}
        /> */}
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.products}
          style={{width: '100%'}}
          keyExtractor={(item, index) => index}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => this.handleOnPressProductList(item.slug)}>
              <View
                style={{
                  flexDirection: 'column',
                  width: '100%',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                  }}>
                  <View style={{width: '30%'}}>
                    <Image
                      style={{
                        width: 80,
                        height: 80,
                        margin: 5,
                        borderRadius: 5,
                        alignContent: 'center',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      source={{uri: item.image.medium}}
                    />
                  </View>
                  <View style={{marginTop: 11, width: '70%'}}>
                    <Text
                      style={{
                        fontSize: 10,
                        fontWeight: '100',
                        color: '#888888',
                        fontFamily: 'lucida grande',
                      }}>
                      {' '}
                      {item.sku}
                    </Text>
                    <View
                      style={{
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        marginRight: 45,
                      }}>
                      <Text
                        style={{
                          width: 166,
                          color: '#444444',
                          fontFamily: 'lucida grande',
                          fontWeight: '500',
                          fontSize: 17,
                        }}>
                        {''}
                        {item.title_en}
                      </Text>
                      <Text
                        style={{
                          fontWeight: '600',
                          fontFamily: 'lucida grande',
                          color: '#444444',
                          fontSize: 17,
                        }}>
                        ₹{item.prices[0].total}
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        marginRight: 21,
                        marginTop: 5,
                      }}>
                      <View
                        style={{
                          paddingBottom: 18,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderColor: '#4267B2',
                          borderWidth: 1,
                          borderRadius: 15,
                          width: 160,
                          height: 26,
                        }}>
                        <Text>{item.variation}</Text>
                      </View>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 1,
                        marginTop: 11,
                        backgroundColor: '#dddddd',
                      }}
                    />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    // alignItems:'center'
  },
});
