import 'react-native-gesture-handler';
import React, {Component} from 'react';

import Header from 'react-native-elements';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  AsyncStorage,
} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

import Login from './src/screens/Login';
import Register from './src/screens/register';
import Splash from './src/screens/splash';
// import Profile from './src/screens/Profile';
import DrawerContainer from './src/component/drawerComponent';
import LocationSelection from './src/screens/locationSelect';
import LoadingScreen from './src/screens/loading';
import DetailsPage from './src/screens/productsDetails';

// const Drawer = createDrawerNavigator();

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="Splash" component={Splash} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen
            name="LocationSelection"
            component={LocationSelection}
          />

          <Stack.Screen name="DrawerContainer" component={DrawerContainer} />
          <Stack.Screen name="LoadingScreen" component={LoadingScreen} />
          <Stack.Screen name="DetailsPage" component={DetailsPage} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

// const styles = {
//   container: {
//     backgroundColor: '#fafafa',
//   },
// };
